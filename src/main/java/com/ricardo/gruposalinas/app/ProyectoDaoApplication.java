package com.ricardo.gruposalinas.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoDaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoDaoApplication.class, args);
	}

}
